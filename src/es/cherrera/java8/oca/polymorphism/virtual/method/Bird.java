package es.cherrera.java8.oca.polymorphism.virtual.method;

public class Bird {
	public String getName() {
		return "Unknown";
	}

	public void displayInformation() {
		System.out.println("The bird name is: " + getName());
	}
}
