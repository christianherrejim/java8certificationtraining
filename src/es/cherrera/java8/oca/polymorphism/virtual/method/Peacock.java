package es.cherrera.java8.oca.polymorphism.virtual.method;

public class Peacock extends Bird {
	public String getName() {
		return "Peacock";
	}

	public static void main(String[] args) {
		Bird bird = new Peacock();
		bird.displayInformation();
	}
}
