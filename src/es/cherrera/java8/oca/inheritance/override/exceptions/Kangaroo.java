package es.cherrera.java8.oca.inheritance.override.exceptions;

import java.io.IOException;

public class Kangaroo extends Marsupial {

	//An overridden method is not required to throw a checked exception defined in the parent class
	public void ex() {
		System.out.println("ex in Kangaroo");
	}

	public static void main(String[] args) {
		Kangaroo joey = new Kangaroo();
		joey.ex();

		Marsupial marsu = new Kangaroo();
		
		// Need try-catch
		try {
			marsu.ex();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
