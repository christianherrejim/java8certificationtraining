package es.cherrera.java8.oca.inheritance.override.exceptions;

import java.io.IOException;

public class Marsupial {
	
	public void ex() throws IOException{
		System.out.println("ex in Marsu");
	}
}
