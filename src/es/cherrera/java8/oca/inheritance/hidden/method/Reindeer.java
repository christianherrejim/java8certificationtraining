package es.cherrera.java8.oca.inheritance.hidden.method;

class Reindeer extends Deer {
	public Reindeer(int age) {
		System.out.print("Reindeer");
	}

	// No lo está sobreescribiendo porque el método en el padre es privado
	public boolean hasHorns() {
		return true;
	}
}
