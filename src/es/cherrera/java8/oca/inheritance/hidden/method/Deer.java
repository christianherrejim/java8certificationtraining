package es.cherrera.java8.oca.inheritance.hidden.method;

public class Deer {
	public Deer() {
		System.out.print("Deer");
	}

	public Deer(int age) {
		System.out.print("DeerAge");
	}

	//hidden version
	private boolean hasHorns() {
		return false;
	}

	public static void main(String[] args) {
		Deer deer = new Reindeer(5);
		System.out.println("," + deer.hasHorns());
	}
}
