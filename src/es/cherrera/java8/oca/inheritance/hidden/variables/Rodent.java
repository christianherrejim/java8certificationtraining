package es.cherrera.java8.oca.inheritance.hidden.variables;

public class Rodent {
	protected int tailLength = 4;

	public void getRodentDetails() {
		System.out.println("[parentTail=" + tailLength + "]");
	}
}
