package es.cherrera.java8.oca.inheritance.hidden.variables;

public class Mouse extends Rodent {
	protected int tailLength = 8;

	public void getMouseDetails() {
		System.out.println("[tail=" + tailLength + ",parentTail=" + super.tailLength + "]");
	}

	public static void main(String[] args) {
		
		Mouse mouse = new Mouse();
		//Java doesn’t allow variables to be overridden but instead hidden.
		mouse.getRodentDetails();
		mouse.getMouseDetails();
	}
}
