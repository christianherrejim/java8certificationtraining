package es.cherrera.java8.oca.inheritance.hiddenstatic.method;

public class Marsupial {
	public static boolean isBiped() {
		return false;
	}

	public void getMarsupialDescription() {
		System.out.println("Marsupial walks on two legs: " + isBiped());
	}
}
